from flask import Flask, render_template
from flask_socketio import SocketIO
import json
from vosk import Model, KaldiRecognizer
import os
import threading
from text_to_num import text2num
import time

from engineio.payload import Payload

import eventlet

eventlet.monkey_patch()

Payload.max_decode_packets = 500

app = Flask(__name__)
app.config["SECRET_KEY"] = "thisissecrets"
socketio = SocketIO(app, async_mode="eventlet")

TIMEUNITS = {"seconde": 1, "secondes": 1, "minute": 60, "minutes": 60,
             "heure": 3600, "heures": 3600, "jour": 86400, "jours": 86400}

if not os.path.exists("model"):
    print("Please download the model from https://alphacephei.com/vosk/models and unpack as 'model' in the current folder.")
    exit(1)

model = Model("model")
rec = KaldiRecognizer(model, 16000)


@app.route("/")
def sessions():
    return render_template("session.html")


def messageReceived(methods=["GET", "POST"]):
    print("message was received!!!")


@socketio.on("on-text")
def on_text(data):
    glorious_ia(data["text"])


# @socketio.on("connect")
# def on_connect():
#    socketio.emit("answer", {"text": "Je vous écoute"})


@socketio.on("on-audio")
def on_audio(data):
    """Write a chunk of audio from the client."""
    if len(data) < 10:
        return

    try:
        if rec.AcceptWaveform(data):
            res = json.loads(rec.Result())
            print(res)
            if res["text"]:
                glorious_ia(res["text"])
    except Exception as err:
        print(err)


def create_reminder(duration, unit):
    time.sleep(TIMEUNITS[unit] * duration)
    socketio.emit("answer", {"text": "Ca fait %s %s !" % (duration, unit)})


def glorious_ia(msg):
    import re

    a = re.match(".* minuteur \w* (\w*) (\w*)", msg)
    if a:
        duration, unit = a.groups()
        duration = text2num(duration, "fr")
        if unit.lower() in TIMEUNITS.keys():
            socketio.emit(
                "answer", {"text": "Ok je creer un minuteur de %s %s." % (duration, unit)})
            create_reminder(duration, unit.lower())
    else:
        socketio.emit("answer", {"text": "Je n'ai pas compris : %s" % msg})


if __name__ == "__main__":
    socketio.run(app, host="0.0.0.0", debug=True)
